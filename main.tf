
# Create a VPC
resource "aws_vpc" "kojitechsvpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = var.vpc_tag
  }
}

#private subnet
resource "aws_subnet" "private_sub" {
  vpc_id     = aws_vpc.kojitechsvpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "private_sub"
  }
}

#public subnet
resource "aws_subnet" "public_sub" {
  vpc_id     = aws_vpc.kojitechsvpc.id
  cidr_block = "10.0.0.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "public_sub"
  }
}